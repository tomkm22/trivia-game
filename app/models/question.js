var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Answer = require('./answer');
var autoIncrement = require('mongoose-auto-increment');
var connection = mongoose.createConnection('mongodb://localhost/trivia-game');
autoIncrement.initialize(connection);

var questionSchema = new Schema({
  question: {
    type: String,
    required: true
  },
  category: String,
  correctAnswer: {
    type: Number,
    ref: 'Answer'
  },
  incorrectAnswers: [{
    type: Number,
    ref: 'Answer'
  }],
  type: {
    type: String,
    enum: ['multiple-choice', 'true/false'],
    required: true
  },
  __v: {
    type: Number,
    select: false
  }
});

questionSchema.plugin(autoIncrement.plugin, 'Question');
module.exports = mongoose.model('Question', questionSchema);
