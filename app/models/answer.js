var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var autoIncrement = require('mongoose-auto-increment');
var connection = mongoose.createConnection('mongodb://localhost/trivia-game');
autoIncrement.initialize(connection);

var answerSchema = new Schema({
  answer: {
    type: String,
    required: true
  },
  __v: {
    type: Number,
    select: false
  }
});

answerSchema.plugin(autoIncrement.plugin, 'Answer');
module.exports = mongoose.model('Answer', answerSchema);
