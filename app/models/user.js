var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var autoIncrement = require('mongoose-auto-increment');

var connection = mongoose.createConnection('mongodb://localhost/trivia-game');
autoIncrement.initialize(connection);

var userSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  score: Number,
  __v: {
    type: Number,
    select: false
  }
});

userSchema.plugin(autoIncrement.plugin, 'User');
module.exports = mongoose.model('User', userSchema);
