// questionController.js

// Grab question schema
var Question = require('../models/question');

module.exports = {
  // Get all questions
  getAllQuestions: function(req, res) {
    var query = {};
    var queries = req.query;

    if (queries.category) {
      query.category = queries.category;
    }
    if (queries.type) {
      query.type = queries.type;
    }

    Question.find(query).
    populate('correctAnswer').
    populate('incorrectAnswers').
    exec(function(err, questions) {
      if (err) {
        res.status(400);
        res.send(err);
      }
      res.send(questions);
    });
  },

  // Get a question
  getQuestion: function(req, res) {
    Question.findById(req.params.question_id).
    populate('correctAnswer').
    populate('incorrectAnswers').
    exec(function(err, question) {
      if (err) {
        res.status(404);
        res.send(err);
      }
      else
        res.send(question);
    });
  },

  // Create a question
  postQuestion: function(req, res) {
    var object = req.body;
    var question = new Question({
      question: object.question,
      category: object.category,
      type: object.type,
      correctAnswer: parseInt(object.correctAnswer),
      incorrectAnswers: object.incorrectAnswers
    });

    question.save(function(err) {
      if (err) {
        res.status(400);
        res.send(err);
      }
      else {
        res.status(201);
        res.send(question);
      }
  },

  // Edit a question
  editQuestion: function(req, res) {
    Question.findById(req.params.question_id, function(err, question) {
      if (err) {
        res.status(404);
        res.send(err);
      }
      var object = req.body;
      if (object.question)
        question.question = object.question;
      if (object.category)
        question.category = object.category;
      if (object.correctAnswers)
        question.correctAnswer = parseInt(object.correctAnswer);
      if (object.incorrectAnswers)

      question.save(function(err) {
        if (err) {
          res.status(400);
          res.send(err);
        }
      });
    });
  },

  // Delete a question
  deleteQuestion: function(req, res) {
    Question.remove({
      _id: req.params.question_id
    },
    function(err, question) {
      if (err) {
        res.status(400);
        res.send(err);
      }
      else
        res.send({
          message: 'Succesfully deleted question!'
        });
    });
  }
}
