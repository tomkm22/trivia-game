// answerController.js

// Grab answer schema
var Answer = require('../models/answer');

module.exports = {
  // Get all answers
  getAllAnswers: function(req, res) {
    Answer.find().
    exec(function(err, answers) {
      if (err) {
        res.status(400);
        res.send(err);
      }
      res.send(answers);
    });
  },

  // Get an answer
  getAnswer: function(req, res) {
    Answer.findById(req.params.answer_id).
    exec(function(err, answer) {
      if (err) {
        res.status(404);
        res.send(err);
      }
      res.send(answer);
    });
  },

  // Create an answer
  postAnswer: function(req, res) {
    var answer = new Answer({
      answer: req.body.answer
    });

    answer.save(function(err) {
      if (err) {
        res.status(400);
        res.send(err);
      }
      else {
        res.status(201);
        res.send(answer);
      }
    })
  },

  // Edit an answer
  editAnswer: function(req, res) {
    Answer.findById(req.params.answer_id, function(err, answer) {
      if (err) {
        res.status(404);
        res.send(err);
      }

      if (req.body.answer)
        answer.answer = req.body.answer;

      answer.save(function(err) {
        if (err) {
          res.status(400);
          res.send(err);
        }
        res.send(answer);
      });
    });
  },

  // Delete an answer
  deleteAnswer: function(req, res) {
    Answer.remove({
      _id: req.params.answer_id
    },
    function(err, answer) {
      if (err) {
        res.status(404);
        res.send(err);
      }
      res.send({
        message: 'Succesfully deleted answer!'
      });
    });
  }
}
