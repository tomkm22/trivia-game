// routes.js

// BASE SETUP
var express = require('express');
var router = express.Router();

// Controllers
var userCtrl = require('./userController.js');
var answerCtrl = require('./answerController.js');
var questionCtrl = require('./questionController.js');

// Logging happens here
router.use(function(req, res, next) {
  console.log('Working on request: ' + req.path);
  console.log('Status code sent: ' + res.statusCode);
  next();
});

router.get('/', function(req, res) {
  res.json({
    message: 'This api works!'
  });
});

// USER ROUTES
// Routes that end in /user
router.route('/user')
  .get(userCtrl.getAllUsers)
  .post(userCtrl.postUser)

// Routes that end in /user/:user_id
router.route('/user/:user_id')
  .get(userCtrl.getUser)
  .put(userCtrl.editUser)
  .delete(userCtrl.deleteUser)

// QUESTION ROUTES
// Routes that end in /question
router.route('/question')
  .get(questionCtrl.getAllQuestions)
  .post(questionCtrl.postQuestion)

// Routes that end in /question/:question_id
router.route('/question/:question_id')
  .get(questionCtrl.getQuestion)
  .put(questionCtrl.editQuestion)
  .delete(questionCtrl.deleteQuestion)

  // Answer ROUTES
  // Routes that end in /answer
  router.route('/answer')
    .get(answerCtrl.getAllAnswers)
    .post(answerCtrl.postAnswer)

  // Routes that end in /answer/:answer_id
  router.route('/answer/:answer_id')
    .get(answerCtrl.getAnswer)
    .put(answerCtrl.editAnswer)
    .delete(answerCtrl.deleteAnswer)

module.exports = router;
