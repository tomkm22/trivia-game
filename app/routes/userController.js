// userController.js

// Grab user schema
var User = require('../models/user');
module.exports = {
  // Get all users
  getAllUsers: function(req, res) {
    var query = {};
    var queries = req.query;

    if (queries.name) {
      query.name = queries.name;
    }
    if (queries.score) {
      query.score = queries.score;
    }

    User.find(query).
    exec(function(err, users) {
      if (err) {
        res.status(400);
        res.send(err);
      }
      res.send(users);
    });
  },

  // Get a user
  getUser: function(req, res) {
    User.findById(req.params.user_id).
    exec(function(err, user) {
      if (err) {
        res.status(404);
        res.send(err);
      }
      res.send(user);
    });
  },

  // Edit a user
  editUser: function(req, res) {
    User.findById(req.params.user_id, function(err, user) {
      if (err) {
        res.status(404);
        res.send(err);
      }
      if (req.body.name)
        user.name = req.body.name;
      if (req.body.score)
        user.score = req.body.score;

      user.save(function(err) {
        if (err) {
          res.status(400);
          res.send(err);
        }
        res.send(user);
      });
    });
  },

  // Create a new user
  postUser: function(req, res) {
    var user = new User({
      name: req.body.name,
      score: req.body.score
    });

    user.save(function(err) {
      if (err) {
        res.status(400);
        res.send(err);
      }
      else {
        res.status(201);
        res.send(user);
      }
    });
  },
  // Delete user
  deleteUser: function(req, res) {
    User.remove({
        _id: req.params.user_id
      },
      function(err, user) {
        if (err) {
          res.status(404);
          res.send(err);
        }
        res.send({
          message: 'Succesfully deleted user!'
        });
      });
  }
}
