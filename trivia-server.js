// server.js

// Base Setup

var express = require('express');
var app = express();
var bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());

// Port 3001
var port = process.env.PORT || 3001;

// Mongoose
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/trivia-game', {useMongoClient: true});

// Routes
var routes = require('./app/routes/routes');

// use /api
app.use('/api', routes);

// Start the Server
app.listen(port)
console.log('Server runs on port ' + port);
